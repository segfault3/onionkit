from onionkit import _


class Status(object):
    INITIALIZING = _("Initializing")

    NOT_INSTALLED = _("Not installed")
    INSTALLING = _("Installing")
    UNINSTALLING = _("Uninstalling")

    STARTING = _("Starting")
    RESTARTING = _("Restarting")
    RUNNING = _("Running")
    STOPPING = _("Stopping")
    STOPPED = _("Stopped")
    STOPPED_UNEXPECTEDLY = _("Stopped unexpectedly")

    PUBLISHING = _("Announcing onion address")

    ERROR_TOR_IS_NOT_RUNNING = _("Tor is not running")
    ERROR = _("An error occurred. See the log for details.")
    INVALID = _("The service is in an invalid state. See the log for details.")
