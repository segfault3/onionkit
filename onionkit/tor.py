from logging import getLogger
import time
import queue
from contextlib import contextmanager

import stem
from stem.control import Controller, EventType

from onionkit.util import process_mainloop_events

logger = getLogger()

PUBLICATION_TIMEOUT = 45


class TorException(Exception):
    pass


class TorManager(object):
    def __init__(self, progress_callback: callable):
        self.progress_callback = progress_callback
        try:
            self.controller = Controller.from_socket_file()
        except stem.SocketError:
            logger.error("Failed to connect to Tor control socket")
            raise
        self.controller.authenticate()
        self.hs_desc_queue = queue.Queue()
        self.publishing = False

    @contextmanager
    def hs_desc_listener(self):
        def hs_desc_listener(event):
            self.hs_desc_queue.put(event)
        self.controller.add_event_listener(hs_desc_listener, EventType.HS_DESC)
        try:
            yield
        finally:
            self.controller.remove_event_listener(hs_desc_listener)

    def start_hidden_service(self, key: str, address: str, virtual_port: int, port: int):
        self.publishing = True
        try:
            self._start_hidden_service(key, address, virtual_port, port)
        finally:
            self.publishing = False

    def _start_hidden_service(self, key_content: str, address: str, virtual_port: int, port: int):

        if not self.tor_circuit_established():
            raise TorException("No Tor circuit established")

        key_type = "RSA1024"

        with self.hs_desc_listener():
            response = self.controller.create_ephemeral_hidden_service(
                ports={virtual_port: port},
                key_type=key_type,
                key_content=key_content,
                discard_key=False,
                detached=True,
                await_publication=False
            )

            if response.service_id:
                response_address = response.service_id + ".onion"
                if address != response_address:
                    raise TorException("Onion address %r does not match precalculated address %r" %
                                       (response_address, address))

            self._wait_for_publication(response)

    def stop_hidden_service(self, address: str):
        self.controller.remove_ephemeral_hidden_service(address.replace(".onion", ""))

    def _wait_for_publication(self, response):
        dirs_uploaded_to = list()
        failures = list()

        start_time = loop_time = time.perf_counter()
        while loop_time - start_time < PUBLICATION_TIMEOUT:
            # Uploading a descriptor should take 30 seconds (see Tor ticket #20082)
            if loop_time - start_time < 30:
                self.progress_callback(100 / 30 * (loop_time - start_time))

            try:
                event = self.hs_desc_queue.get(timeout=0)
                logger.debug("New HS_DESC event: %s, action: %s, reason: %s", event, event.action, event.reason)

                # This code is from stem.control (v1.5.4)
                if event.action == stem.HSDescAction.UPLOAD and event.address == response.service_id:
                    dirs_uploaded_to.append(event.directory_fingerprint)
                elif event.action == stem.HSDescAction.UPLOADED and event.directory_fingerprint in dirs_uploaded_to:
                    return  # successfully uploaded to a HS authority... maybe
                elif event.action == stem.HSDescAction.FAILED and event.directory_fingerprint in dirs_uploaded_to:
                    failures.append('%s (%s)' % (event.directory_fingerprint, event.reason))

                    if len(dirs_uploaded_to) == len(failures):
                        raise TorException('Failed to upload our hidden service descriptor to %s' % ', '.join(failures))
            except queue.Empty:
                for i in range(100):
                    process_mainloop_events()
                    time.sleep(0.01)
            loop_time = time.perf_counter()

        self.controller.remove_ephemeral_hidden_service(response.service_id)
        raise TorException("Timeout reached while trying to upload hidden service descriptor")

    def check_hidden_service_published(self, address: str):
        # The service id is already in onions/detached when we wait for publication,
        # even though we don't know if the publication is successful.
        # So we return False if we are currently waiting for publication.
        if self.publishing:
            return False

        service_id = address.replace(".onion", "")
        try:
            published_service_ids = self.controller.get_info("onions/detached").split("\n")
            return service_id in published_service_ids
        except stem.ProtocolError as e:
            if e.args == stem.ProtocolError("GETINFO response didn't have an OK status:\n"
                                            "No onion services of the specified type.").args:
                return False
            logger.error("Got a ProtocolError from stem", exc_info=True)

    def tor_circuit_established(self):
        return bool(int(self.controller.get_info("status/circuit-established")))
