from logging import getLogger
from contextlib import contextmanager
from pathlib import Path
import shutil
import time
import subprocess

import sh

from onionkit import _
from onionkit import DEBUG
from onionkit import CONTAINER_BASE, MACHINE_DIR, SYSTEMD_UNIT_DIR

from typing import List

logger = getLogger(__name__)


START_TIMEOUT = 5
# This will be waited for twice, once when trying to stop container gracefully
# and once after sending SIGKILL to all container processes
STOP_TIMEOUT = 5
APT_UPDATE_TIMEOUT = 60
APT_INSTALL_TIMEOUT = 60
OVERLAY_MOUNT_TIMEOUT = 5
OVERLAY_UNMOUNT_TIMEOUT = 5


class ContainerError(Exception):
    pass


class CommandFailedError(Exception):
    pass


class APTError(Exception):
    pass


UNIT_FILE_CONTENT = str(
    "[Service]\n"
    "ExecStart=/usr/bin/systemd-nspawn --quiet "
    "--keep-unit "
    # Boot systemd
    "--boot "
    # Make guest journal visible to host
    "--link-journal=try-guest "
    # Execute container with its own user namespace, increasing security. This changes the owner
    # and group of the rootfs recursively, which means that the whole upperdir gets copied to the
    # lowerdir, which nullifies the memory advantage of overlayfs and takes ~1 min during first
    # container startup.
    # XXX: Instead, use fixed UID and GID for all containers, and chown -R the base rootfs
    # "-U "
    # Don't use settings from .nspawn files
    "--settings=false "
    # Notify the host systemd when the guest systemd is ready
    "--notify-ready=yes "
    # Use the unit instance name as the machine name for this container
    "--machine=%i"
)


class ContainerManager(object):
    def __init__(self, service_name: str,
                 service_state_dir: Path,
                 status_callback: callable,
                 progress_callback: callable):
        self.name = "onionkit-" + service_name
        self.status_callback = status_callback
        self.progress_callback = progress_callback
        self.lowerdir = Path(CONTAINER_BASE)
        self.upperdir = Path(service_state_dir, "overlay_rootfs")
        self.workdir = Path(service_state_dir, "overlay_workdir")
        self.machine_dir = Path(MACHINE_DIR, self.name)
        self.unit_file = Path(SYSTEMD_UNIT_DIR, "systemd-nspawn@%s.service" % self.name)

    def create(self):
        if not self.lowerdir.exists():
            # The base rootfs should have been created via debootstrap in setup.py
            raise ContainerError("Cannot create %r: base directory %r does not exist" % (self.name, self.lowerdir))

        self.unit_file.touch(exist_ok=True)
        self.unit_file.write_text(UNIT_FILE_CONTENT)
        self.upperdir.mkdir(parents=True, exist_ok=True)
        self.workdir.mkdir(parents=True, exist_ok=True)
        self.machine_dir.mkdir(parents=True, exist_ok=True)

    def start(self):
        logger.debug("Starting %r", self.name)

        if not self.unit_file.exists():
            raise ContainerError("Cannot start %r: unit file does not exist" % self.name)

        self.mount_overlay()

        sh.machinectl("start", self.name)

        start_time = time.perf_counter()
        while time.perf_counter() - start_time < START_TIMEOUT:
            if self.is_ready():
                logger.debug("Successfully started %r", self.name)
                return
            time.sleep(0.2)

        raise ContainerError("Failed to start %r (timeout %s)" % (self.name, START_TIMEOUT))

    def stop(self):
        def do_stop():
            logger.debug("Stopping %r", self.name)

            if not self.is_running():
                logger.warning("Cannot stop container %r: container is not running", self.name)
                return

            sh.machinectl("stop", self.name)

            start_time = time.perf_counter()
            while time.perf_counter() - start_time < STOP_TIMEOUT:
                if not self.is_running():
                    return
                time.sleep(0.2)

            logger.warning("Failed to stop %r gracefully (timeout %s). Sending SIGKILL to all container processes.",
                           self.name, STOP_TIMEOUT)
            sh.machinectl("kill", "--signal=SIGKILL", self.name)

            start_time = time.perf_counter()
            while time.perf_counter() - start_time < STOP_TIMEOUT:
                if not self.is_running():
                    return
                time.sleep(0.2)

            raise ContainerError("Failed to stop %r (timeout %s)" % (self.name, STOP_TIMEOUT))

        do_stop()

        if self.overlay_is_mounted():
            self.unmount_overlay()

    def destroy(self):
        logger.debug("Destroying %r", self.name)

        if self.overlay_is_mounted():
            self.unmount_overlay()

        if self.is_running():
            self.stop()

        if self.exists():
            # This should remove the machine dir
            sh.machinectl("remove", self.name)
        else:
            logger.warning("Could not remove machine %r: machine does not exist", self.name)

        if self.unit_file.exists():
            self.unit_file.unlink()
        else:
            logger.warning("Cannot remove unit file '%s': file does not exist", self.unit_file)

        if self.upperdir.exists():
            shutil.rmtree(str(self.upperdir))
        else:
            logger.warning("Cannot remove directory '%s': directory does not exist", self.upperdir)

    def mount_overlay(self):
        # XXX: Use the systemd-nspawn overlay option once this is fixed:
        # https://github.com/systemd/systemd/issues/3847
        logger.debug("Mounting overlay of %r", self.name)

        # Ensure that the overlay is not already mounted. It might also be mounted multiple times,
        # so we use a loop here.
        start_time = time.perf_counter()
        while time.perf_counter() - start_time < OVERLAY_UNMOUNT_TIMEOUT:
            if not self.overlay_is_mounted():
                break
            logger.warning("Overlay of %r is already mounted. Trying to unmount", self.name)
            self.unmount_overlay()

        if self.overlay_is_mounted():
            raise ContainerError("Failed to mount overlay of %r: Overlay is already mounted and cannot be unmounted "
                                 "(timeout: %s)" % (self.name, OVERLAY_UNMOUNT_TIMEOUT))

        sh.mount("-t", "overlay", "overlay",
                 "-olowerdir=%s,upperdir=%s,workdir=%s" % (self.lowerdir, self.upperdir, self.workdir),
                 str(self.machine_dir))

        # Ensure that the overlay is mounted
        start_time = time.perf_counter()
        while time.perf_counter() - start_time < OVERLAY_MOUNT_TIMEOUT:
            if self.overlay_is_mounted():
                return
            time.sleep(0.1)

        raise ContainerError("Failed to mount overlay of %r (timeout: %s)", self.name, OVERLAY_MOUNT_TIMEOUT)

    def unmount_overlay(self):
        sh.umount("-f", str(self.machine_dir))

    def overlay_is_mounted(self) -> bool:
        try:
            sh.findmnt(str(self.machine_dir))
            return True
        except sh.ErrorReturnCode_1:
            return False

    def install_packages(self, packages: List[str]):
        logger.debug("Installing packages in %r", self.name)
        # XXX: Use `apt-get -o APT::Status-Fd` to get progress
        self.status_callback(_("Updating package cache"))
        self.execute_apt("update", timeout=APT_UPDATE_TIMEOUT)
        self.status_callback(_("Downloading packages"))
        self.execute_apt("install -y " + ' '.join(packages), timeout=APT_INSTALL_TIMEOUT)

    def bind(self, source: str, target: str):
        logger.debug("Bind mounting %r in %r", target, self.name)
        sh.machinectl("bind", "--mkdir", self.name, source, target)

    def is_running(self) -> bool:
        return container_is_running(self.name)

    def is_ready(self) -> bool:
        try:
            self.execute_command("/bin/true")
            return True
        except CommandFailedError:
            return False

    def exists(self) -> bool:
        try:
            sh.machinectl("show-image", self.name)
            return True
        except sh.ErrorReturnCode_1:
            return False

    def execute_command(self, command: str, timeout=30, handle_stderr_line: callable = None) -> str:
        logger.debug("Executing command in %r: '%s'", self.name, command)
        exit_code = None
        p = subprocess.Popen(["systemd-run", "--pipe", "--wait", "-M", self.name, "/bin/sh", "-c", command],
                             stdout=subprocess.PIPE,
                             stderr=subprocess.PIPE,
                             universal_newlines=True,
                             bufsize=1)

        start_time = time.perf_counter()
        while time.perf_counter() - start_time < timeout:
            exit_code = p.poll()
            if exit_code is not None:
                break
            if handle_stderr_line:
                handle_stderr_line(p.stderr.readline())
            time.sleep(0.2)

        if exit_code is None:
            raise CommandFailedError("Timeout while running command '%s' (timeout %s) stderr: %s" %
                                     (command, timeout, p.stderr.read()))

        if handle_stderr_line:
            for l in p.stderr.readlines():
                handle_stderr_line(l)

        if exit_code != 0:
            raise CommandFailedError("Command '%s' failed with exit code %s. stderr: %s" %
                                     (command, exit_code, p.stderr.read()))

        return p.stdout.read()

    def execute_apt(self, command: str, timeout):

        def handle_stderr_line(line):
            line = line.strip()
            if not line:
                return
            logger.debug("APT stderr: %r", line)

            # Parse APT error
            if line.startswith('E:'):
                error_message = line[2:]
                raise APTError("APT command '%s' failed: %s" % (command, error_message))

            # Parse APT status information
            # https://github.com/mvo5/apt/blob/master/README.progress-reporting
            info = line.split(':')
            if info[0] == "dlstatus":
                self.progress_callback(int(info[2].split('.')[0]))
            elif info[0] == "pmstatus":
                self.status_callback(info[3])

        command = "/usr/bin/apt-get --option=APT::Status-Fd=2 " + command
        self.execute_command(command, timeout=timeout, handle_stderr_line=handle_stderr_line)

    @contextmanager
    def run(self):
        self.start()
        try:
            yield
        except Exception as e:
            if DEBUG:
                logger.exception(e)
                input("Paused to allow debugging. Press Enter to continue")
            raise
        finally:
            self.stop()


def container_is_running(container: str) -> bool:
    try:
        sh.machinectl("show", container)
    except sh.ErrorReturnCode_1:
        # Container is not running
        return False

    out = sh.systemctl("--no-pager", "show", "systemd-nspawn@%s.service" % container, "-p", "ActiveState")
    state = out.replace("ActiveState=", "").strip()
    if state in ("inactive", "failed"):
        logger.warning("machinectl incorrectly states that the container is running. Terminating machine.")
        sh.machinectl("terminate", container)
        return False

    return True
