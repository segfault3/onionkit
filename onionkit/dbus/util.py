from typing import Any

from gi.repository import GLib


class InvalidOptionTypeError(Exception):
    pass


def option_to_variant(value: Any) -> GLib.Variant:
    if isinstance(value, str):
        return GLib.Variant("s", value)
    # It is important that we test bool before int, because isinstance(bool(), int) is True
    if isinstance(value, bool):
        return GLib.Variant("b", value)
    if isinstance(value, int):
        return GLib.Variant("i", value)
    raise InvalidOptionTypeError("Unsupported option type %r" % type(value))
