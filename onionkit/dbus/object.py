from logging import getLogger
from typing import Union
from threading import Thread
from abc import abstractmethod

from gi.repository import Gio, GLib

from onionkit.dbus.util import option_to_variant
from onionkit.dbus.bus import bus


logger = getLogger(__name__)


class RegistrationFailedError(Exception):
    pass


class UnregistrationFailedError(Exception):
    pass


class DBusObject(object):
    @property
    @abstractmethod
    def dbus_info(self) -> str:
        pass

    @property
    @abstractmethod
    def dbus_path(self) -> str:
        pass

    def __init__(self):
        self.node_info = Gio.DBusNodeInfo.new_for_xml(self.dbus_info)
        self.reg_ids = list()
        self.registered = False

    def register(self):
        logger.debug("Registering %r", self.dbus_path)

        for interface in self.node_info.interfaces:
            reg_id = bus.connection.register_object(self.dbus_path,
                                                    interface,
                                                    self.handle_method_call,
                                                    self.handle_get_property,
                                                    self.handle_set_property)
            if not reg_id:
                raise RegistrationFailedError("Failed to register interface %r of object %r" % (interface, self))

            self.reg_ids.append(reg_id)

        self.registered = True

    def unregister(self):
        logger.debug("Unregistering %r", self.dbus_path)
        for reg_id in self.reg_ids:
            unregistered = bus.connection.unregister_object(reg_id)
            if not unregistered:
                raise UnregistrationFailedError("Failed to unregister object %r" % self)
        self.reg_ids = list()
        self.registered = False

    def emit_signal(self, interface_name, signal_name, parameters=None, signature=None):
        if not self.registered:
            logger.debug("Could not emit signal %r: Object %r not registered with DBus",
                         signal_name, self.__class__.__name__)
            return
        if parameters:
            parameters = GLib.Variant("(%s)" % signature, (parameters,))
        bus.connection.emit_signal(None, self.dbus_path, interface_name, signal_name, parameters)

    def handle_method_call(self, *args, **kwargs) -> None:
        thread = Thread(target=self.do_handle_method_call, args=args, kwargs=kwargs, daemon=True)
        thread.start()

    def do_handle_method_call(self,
                              connection: Gio.DBusConnection,
                              sender: str,
                              object_path: str,
                              interface_name: str,
                              method_name: str,
                              parameters: GLib.Variant,
                              invocation: Gio.DBusMethodInvocation,
                              user_data: Union[object, None] = None) -> None:
        try:
            logger.debug("Handling method call %s.%s%s", self.__class__.__name__, method_name, parameters)
            method_info = self.node_info.lookup_interface(interface_name).lookup_method(method_name)

            func = getattr(self, method_name)
            result = func(*parameters)

            if not method_info.out_args:
                invocation.return_value(None)
                return

            if len(method_info.out_args) == 1:
                result = (result,)

            return_signature = "(%s)" % "".join(arg.signature for arg in method_info.out_args)
            invocation.return_value(GLib.Variant(return_signature, result))
        except Exception as e:
            logger.exception(e)
            invocation.return_dbus_error("python." + type(e).__name__, str(e))

    def handle_get_property(self,
                            connection: Gio.DBusConnection,
                            sender: str,
                            object_path: str,
                            interface_name: str,
                            property_name: str,
                            user_data: Union[object, None] = None) -> GLib.Variant:
        logger.debug("Handling property read of %s.%s", object_path, property_name)
        try:
            property_info = self.node_info.lookup_interface(interface_name).lookup_property(property_name)
            attribute = getattr(self, property_name)

            if isinstance(attribute, property):
                value = attribute.fget(attribute)
            else:
                value = attribute

            if interface_name == "org.boum.tails.OnionKit.Option" and property_name == "Value":
                value = option_to_variant(value)

            logger.debug("Converting value %r to Variant type %r", value, property_info.signature)
            return GLib.Variant(property_info.signature, value)
        except Exception as e:
            logger.exception(e)

    def handle_set_property(self,
                            connection: Gio.DBusConnection,
                            sender: str,
                            object_path: str,
                            interface_name: str,
                            property_name: str,
                            value: GLib.Variant,
                            # error: GLib.Error,
                            user_data: Union[object, None] = None) -> bool:
        logger.debug("Handling property write of %s.%s", object_path, property_name)
        setattr(self, property_name, value.unpack())
        return True
