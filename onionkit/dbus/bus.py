from logging import getLogger
from gi.repository import Gio

from onionkit import BUS_NAME

logger = getLogger(__name__)


class BusNameLostError(Exception):
    pass


class Bus(object):

    def __init__(self, name):
        self.name = name
        self.connection = Gio.bus_get_sync(Gio.BusType.SYSTEM, None)
        self.connection.autoclose = True
        self.owner_id = None

    def acquire_name(self):
        self.owner_id = Gio.bus_own_name_on_connection(self.connection,
                                                       self.name,
                                                       Gio.BusNameOwnerFlags.ALLOW_REPLACEMENT |
                                                       Gio.BusNameOwnerFlags.REPLACE,
                                                       self.on_name_acquired,
                                                       self.on_name_lost)

    def on_name_acquired(self, connection: Gio.DBusConnection, name: str) -> None:
        logger.info("Acquired bus name %r", name)

    def on_name_lost(self, connection: Gio.DBusConnection, name: str) -> None:
        raise BusNameLostError("Lost bus name %r" % name)


bus = Bus(BUS_NAME)
