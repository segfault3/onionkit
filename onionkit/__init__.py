# --- Config --- #

from pathlib import Path
import os
import glob
import yaml

config_file = Path("/etc/onionkit/onionkit.conf")
with config_file.open() as f:
    config = yaml.load(f)

DATA_DIR = config["data_dir"]
STATE_DIR = config["state_dir"]
CONTAINER_BASE = os.path.join(DATA_DIR, "rootfs")


TEMP_STATE_DIR = "/run/onionkit"
BUS_NAME = "org.boum.tails.OnionKit"
BASE_PATH = "/org/boum/tails/OnionKit"
SERVICES_PATH = BASE_PATH

MACHINE_DIR = "/var/lib/machines"
SYSTEMD_UNIT_DIR = "/lib/systemd/system"

# Indicates whether the current execution is for debugging purposes
DEBUG = False

# --- Translation stuff --- #

import gettext

if os.path.exists('po/locale'):
    translation = gettext.translation('onionservices', 'po/locale', fallback=True)
else:
    translation = gettext.translation('onionservices', '/usr/share/locale', fallback=True)

_ = translation.gettext


# --- Services class to import and access and service modules --- #

import os
from importlib.machinery import SourceFileLoader
from typing import List

SERVICES_DIR = os.path.join(DATA_DIR, "services")


def get_service_classes() -> List[type]:
    classes = list()  # type: List[type]
    paths = glob.glob(os.path.join(SERVICES_DIR, "*.py"))
    paths.sort()

    for path in paths:
        name = os.path.basename(path)
        loader = SourceFileLoader(name, path)
        module_ = loader.load_module()
        classes.append(module_.service_class)
    return classes
