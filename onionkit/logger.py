import logging
import logging.handlers
import colorlog


def initialize(verbosity, log_file):
    logger = colorlog.getLogger()
    logger.setLevel(0)
    logger.propagate = 0

    if not verbosity:
        level = logging.INFO
    elif verbosity == 1:
        level = logging.DEBUG
    elif verbosity == 2:
        level = 5
    else:
        level = 0
    if log_file:
        file_handler = logging.handlers.RotatingFileHandler(log_file, mode="w")
        file_handler.setLevel(5)
        format_ = "%(asctime)s [%(threadName)s] %(name)s: %(levelname)s - %(message)s"
        file_handler.setFormatter(logging.Formatter(format_))
        logger.addHandler(file_handler)
        logging.getLogger('sh').addHandler(file_handler)

    console_handler = colorlog.StreamHandler()
    console_handler.setLevel(level)
    format_ = "%(log_color)s[%(threadName)s] %(name)s:%(levelname)s: %(message)s"
    console_handler.setFormatter(colorlog.ColoredFormatter(
        format_,
        log_colors={
            'DEBUG': 'cyan',
            'INFO': 'green',
            'WARNING': 'yellow',
            'ERROR': 'red',
            'CRITICAL': 'red,bg_white'}
    ))
    logger.addHandler(console_handler)
    logging.getLogger('sh').addHandler(console_handler)

    logging.getLogger('stem').setLevel(level + 5)
    logging.getLogger('sh').setLevel(level + 5)
    logging.getLogger('sh').propagate = False
