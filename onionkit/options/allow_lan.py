import sh
from logging import getLogger

from onionkit import _
from onionkit.option import OnionServiceOption


logger = getLogger(__name__)


class AllowLAN(OnionServiceOption):
    NameForDisplay = _("Allow LAN")
    Description = _("Allow connections from the local network")
    Group = _("Firewall")
    # Default = None means the default is not set during initialization.
    # We don't want a default set during initialization, because there
    # is no rule to be removed.
    Default = False
    type = bool
    is_applied_without_restarting = True

    @property
    def rules(self):
        return [("INPUT", "--source", subnet, "--protocol", "tcp",
                 "--dport", self.service.port, "--jump", "ACCEPT")
                for subnet in ("10.0.0.0/8", "172.16.0.0/12", "192.168.0.0/16")]

    def store(self):
        super().store()
        if self.Value == self.is_allowed():
            logger.debug("Current value of option '%s.%s' is already stored", self.service.Name, self.Name)
            return
        if self.Value:
            self.accept_lan_connections()
        else:
            self.reject_lan_connections()

    def load(self):
        return self.is_allowed()

    def accept_lan_connections(self):
        for rule in self.rules:
            sh.iptables("-I", *rule)

    def reject_lan_connections(self):
        for rule in self.rules:
            sh.iptables("-D", *rule)

    def is_allowed(self):
        return all(self.is_active(rule) for rule in self.rules)

    @staticmethod
    def is_active(rule):
        try:
            sh.iptables("-C", *rule)
            return True
        except sh.ErrorReturnCode_1:
            return False

    def clean_up(self):
        logger.debug("Cleaning up option '%s.%s'", self.service.Name, self.Name)
        if self.Value:
            self.reject_lan_connections()
