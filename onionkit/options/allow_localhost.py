import sh
from logging import getLogger

from onionkit import _
from onionkit.option import OnionServiceOption


logger = getLogger(__name__)


class AllowLocalhost(OnionServiceOption):
    NameForDisplay = _("Allow localhost")
    Description = _("Allow connections from this computer")
    Group = _("Firewall")
    # Default = None means the default is not set during initialization.
    # We don't want a default set during initialization, because there
    # is no rule to be removed.
    Default = False
    type = bool
    is_applied_without_restarting = True

    @property
    def rule(self):
        return ("OUTPUT", "--out-interface", "lo", "--protocol", "tcp",
                "--dport", self.service.port, "--jump", "ACCEPT")

    def store(self):
        super().store()
        if self.Value == self.is_allowed():
            logger.debug("Current value of option '%s.%s' is already stored", self.service.Name, self.Name)
            return
        if self.Value:
            self.accept_localhost_connections()
        else:
            self.reject_localhost_connections()

    def load(self):
        return self.is_allowed()

    def accept_localhost_connections(self):
        sh.iptables("-I", *self.rule)

    def reject_localhost_connections(self):
        sh.iptables("-D", *self.rule)

    def is_allowed(self):
        try:
            sh.iptables("-C", *self.rule)
            return True
        except sh.ErrorReturnCode_1:
            return False

    def clean_up(self):
        super().clean_up()
        if self.Value:
            self.reject_localhost_connections()
