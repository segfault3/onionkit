from onionkit import _
from onionkit.option import OnionServiceOption


class VirtualPort(OnionServiceOption):
    NameForDisplay = _("Port")
    Description = _("Port opened on the Tor network")
    Group = _("Connection")
    type = int

    @property
    def Default(self):
        return self.service.port

    def on_value_changed(self):
        super().on_value_changed()
        self.service.on_connection_info_changed()
