from pathlib import Path
import yaml

from onionkit.data_file import DataFile
from onionkit.exceptions import OptionNotFoundError
from onionkit.util import open_locked


class OptionsFile(DataFile):
    source = Path("options")
    default_content = '{}'

    def get(self, key: str):
        options = yaml.load(self.read())
        try:
            return options[key]
        except KeyError:
            raise OptionNotFoundError("Could not find option %r in %s" % (key, self.source))

    def set(self, key: str, value):
        with open_locked(self.source, 'r+') as f:
            options = yaml.load(f.read())
            options[key] = value
            f.seek(0)
            f.truncate()
            yaml.dump(options, f, default_flow_style=False)
