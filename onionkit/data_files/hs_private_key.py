from pathlib import Path
from logging import getLogger
from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives.asymmetric import rsa
from cryptography.hazmat.primitives import serialization
from hashlib import sha1
from base64 import b32encode, b64encode, b64decode

from onionkit.data_file import DataFile

logger = getLogger(__name__)


# The exponent is hard coded to 65537 in Tor (see tor/common/crypto.c)
PUBLIC_EXPONENT = 65537

# The key size is hard coded to 1024 in Tor (see tor/common/crypto.c)
KEY_SIZE = 1024


class HSPrivateKey(DataFile):
    source = Path("private_key")
    default_content = str()

    def create(self):
        logger.debug("Creating new private key")
        super().create()
        self.generate_hs_private_key()

    def generate_hs_private_key(self):
        private_key = rsa.generate_private_key(
            public_exponent=PUBLIC_EXPONENT,
            key_size=KEY_SIZE,
            backend=default_backend()
        )

        der = private_key.private_bytes(
            encoding=serialization.Encoding.DER,
            format=serialization.PrivateFormat.TraditionalOpenSSL,
            encryption_algorithm=serialization.NoEncryption()
        )

        key = b64encode(der).decode()
        self.write(key)

    def derive_onion_address(self) -> str:
        """Derive the onion address from a hidden service private key according to the torspec
        See https://gitweb.torproject.org/torspec.git/tree/rend-spec.txt#n526"""

        der_private_key = b64decode(self.read().encode())

        private_key = serialization.load_der_private_key(
            der_private_key,
            backend=default_backend(),
            password=None
        )

        public_key = private_key.public_key()

        der_public_key = public_key.public_bytes(
            encoding=serialization.Encoding.DER,
            format=serialization.PublicFormat.PKCS1
        )

        # Calculate SHA1 of the private key
        m = sha1()
        m.update(der_public_key)
        digest = m.digest()

        # Use only the first 80 bits (= 10 bytes)
        truncated_digest = digest[:10]

        # Generate base32 encoding
        base32 = b32encode(truncated_digest)

        address = base32.decode()[:16].lower() + ".onion"
        return address
