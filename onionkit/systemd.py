from logging import getLogger
import sh
import subprocess
from threading import Thread
import time

from onionkit.exceptions import MonitorError
from onionkit.service_status import Status
from onionkit.container import container_is_running

logger = getLogger()


class SystemdError(Exception):
    pass


START_TIMEOUT = 5
STOP_MONITOR_TIMEOUT = 1


class SystemdManager(object):
    """Helper to perform various actions on systemd services via DBus calls to systemd.
    Reference for systemd DBus API: https://www.freedesktop.org/wiki/Software/systemd/dbus/"""

    def __init__(self, name: str, service: str, status_callback: callable):
        self.container = "onionkit-" + name
        self.service = service
        self.status_callback = status_callback
        # Name for identifying the service, only used in log messages
        self.id = '%s/%s' % (self.container, self.service)
        self.monitor = None  # type: subprocess.Popen
        self.parser = None  # type: Thread
        self.restarting = False

    def restart(self):
        logger.debug("Restarting %s", self.id)
        # The restarting attribute tells the monitor to ignore it if the service is stopped
        self.restarting = True
        try:
            sh.systemctl("-M", self.container, "restart", self.service)
        finally:
            self.restarting = False

        # Make sure that the service was successfully started
        start_time = time.perf_counter()
        while time.perf_counter() - start_time < START_TIMEOUT:
            if self.is_running():
                logger.debug("Successfully started %s", self.id)
                return
            time.sleep(0.2)

        raise SystemdError("Failed to start %s (timeout: %s). systemctl status: %s" %
                           (self.id, START_TIMEOUT, self._get_service_status_output()))

    def disable(self):
        logger.debug("Disabling %s", self.id)
        if sh.systemctl("-M", self.container, "show", "-p", "CanStart") != "CanStart=yes":
            # The disable command fails if there is no unit file installed (e.g. because it's not automatically
            # installed by the APT package but by us later), so we do nothing if the unit cannot be started anyway
            logger.debug("Not disabling %s because the unit cannot be started anyway (probably no unit file installed)")
        else:
            sh.systemctl("-M", self.container, "disable", self.service)

    def is_running(self) -> bool:
        if not container_is_running(self.container):
            return False
        active_state, sub_state = self._get_state()
        return active_state == "active" and sub_state == "running"

    def daemon_reload(self):
        logger.debug("Reloading systemd manager configuration of %s", self.container)
        sh.systemctl("-M", self.container, "daemon-reload")

    def start_monitoring(self):
        logger.debug("Start monitoring %s", self.id)

        if self.monitor and self.monitor.poll() is not None:
            logger.warning("Monitor %s is already running. Trying to stop it first.", self.id)
            self.stop_monitoring()

        expression = "type='signal',interface='org.freedesktop.DBus.Properties',member='PropertiesChanged'"
        command = ["busctl", "-M", self.container, "--match", expression, "monitor", self.service]
        logger.debug("Starting watcher with command '%s'", ' '.join(command))
        self.monitor = subprocess.Popen(command,
                                        stdout=subprocess.PIPE,
                                        stderr=subprocess.PIPE,
                                        universal_newlines=True,
                                        bufsize=1)

        self.parser = Thread(target=self._parse_monitor_output, daemon=True)
        self.parser.start()

    def stop_monitoring(self):
        logger.debug("Stopping monitor %s", self.id)

        if not self.monitor:
            logger.warning("Cannot stop monitor %s: Monitor was not started", self.id)
            return

        if self.monitor.poll() is not None:
            logger.warning("Cannot stop monitor %s: Monitor died with exit code %s",
                           self.id, self.monitor.poll())
            return

        self.monitor.kill()
        try:
            self.monitor.wait(STOP_MONITOR_TIMEOUT)
        except subprocess.TimeoutExpired:
            raise MonitorError("Failed to stop monitor %s (timeout: %s)" % (self.id, STOP_MONITOR_TIMEOUT))

    def _parse_monitor_output(self):
        while True:
            exit_code = self.monitor.poll()
            if exit_code == 0:
                # The monitor exited without error, hopefully because we stopped it, so we exit too
                return
            if exit_code:
                # The monitor exited with an error exit code
                logger.error("Monitor %s died with exit code %s. stderr: %s",
                             self.id, self.monitor.poll(), self.monitor.stderr.read())
                self.status_callback(Status.ERROR)
                return

            # This blocks until there is a new line on stdout
            line = self.monitor.stdout.readline()
            logger.log(5, "%r" % line)

            if line.startswith('‣'):
                message = self._read_message(line)
                if 'STRING "ActiveState"' in message or 'STRING "SubState"' in message:
                    self._update_status()
            else:
                logger.warning("Unexpected output from monitor %s: %r", self.id, line)

    def _read_message(self, first_line: str) -> str:
        message = str()
        line = first_line
        while line != '\n':
            message += line
            line = self.monitor.stdout.readline()
            logger.log(5, "%r" % line)
        return message

    def _update_status(self):
        if self.restarting:
            # The service is currently being restarted, so we don't care if the service is being stopped
            return

        active_state, sub_state = self._get_state()
        if active_state in ("inactive", "failed") or sub_state == "exited":
            logger.error("Service '%s' stopped. systemctl status: %s", self.service, self._get_service_status_output())
            self.status_callback(Status.ERROR)

    def _get_state(self) -> (str, str):
        output = sh.systemctl("--no-pager", "-M", self.container, "show", "-p", "ActiveState,SubState", self.service)
        logger.log(5, "Output of `systemctl -M %s show -p ActiveState -p SubState %s`: %s",
                   self.container, self.service, output)
        lines = output.split()
        active_state = lines[0].split('=')[-1]
        sub_state = lines[1].split('=')[-1]
        logger.debug("Service %r state: ActiveState: %r, SubState: %r", self.service, active_state, sub_state)
        return active_state, sub_state

    def _get_service_status_output(self) -> str:
        # Get systemctl status output for debugging
        output = str()
        try:
            output = sh.systemctl("--no-pager", "--lines=0", "-M", self.container,
                                  "status", self.service, _ok_code=[0, 3]).stdout.decode()
            # In the current version (238) systemctl status does not include
            # log lines when using the -M option, so we add those to the output ourselves
            output += '\n' + sh.journalctl("--no-pager", "--lines=10", "-M", self.container,
                                           "_SYSTEMD_UNIT=" + self.service).stdout.decode()
        except sh.ErrorReturnCode:
            # `systemctl status` currently returns exit code 3 if the unit is stopped,
            # but we shouldn't rely on that, so we ignore other error exit codes too
            pass
        return output
