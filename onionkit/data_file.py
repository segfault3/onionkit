from logging import getLogger
import os
from pathlib import Path
from pwd import getpwnam
from grp import getgrnam
from abc import ABCMeta
import configparser

from onionkit.util import open_locked
from onionkit.exceptions import OptionNotFoundError

logger = getLogger(__name__)


class DataFile(object, metaclass=ABCMeta):

    source = None  # type: Path
    default_content = None  # type: str

    owner = "root"
    group = "root"
    mode = 0o640

    def create(self):
        logger.debug("Creating file %s", self.source)

        # Don't set the UID and GID if the file already exists, because it might be a mount point
        # which uses UID and GID from the container
        if not self.source.exists():
            with open_locked(self.source, 'w') as f:
                os.fchown(f.fileno(), uid=getpwnam(self.owner).pw_uid, gid=getgrnam(self.group).gr_gid)
                os.fchmod(f.fileno(), mode=self.mode)

        self.source.write_text(self.default_content)

    def read(self) -> str:
        return self.source.read_text()

    def write(self, s: str) -> int:
        return self.source.write_text(s)

    def delete_lines_starting_with(self, s: str):
        with open_locked(self.source, 'r+') as f:
            lines = [line for line in f.readlines() if not line.startswith(s)]
            f.seek(0)
            f.truncate()
            f.writelines(lines)

    def append_line(self, line: str):
        with open_locked(self.source, 'a') as f:
            f.writelines([line])

    def insert_line(self, line: str):
        with open_locked(self.source, 'r+') as f:
            lines = [line] + f.readlines()
            f.seek(0)
            f.truncate()
            f.writelines(lines)

    def get_first_line_starting_with(self, s: str):
        with open_locked(self.source) as f:
            for line in f.readlines():
                if line.startswith(s):
                    return line


class IniLikeConfigFile(DataFile, metaclass=ABCMeta):
    def __init__(self, *args, **kwargs):
        super().__init__()
        self.config = configparser.ConfigParser(*args, **kwargs)

    def get(self, section: str, key: str) -> str:
        try:
            with open_locked(self.source) as f:
                self.config.read_string(f.read())
                return self.config.get(section, key)
        except (configparser.NoSectionError, configparser.NoOptionError):
            raise OptionNotFoundError("Property %r not found in config file %s" % (key, self.source))

    def set(self, section: str, key: str, value: str):
        try:
            with open_locked(self.source, 'r+') as f:
                self.config.read_string(f.read())
                if not self.config.has_section(section):
                    self.config.add_section(section)
                self.config.set(section, key, value)
                f.seek(0)
                f.truncate()
                self.config.write(f)
        except (configparser.NoSectionError, configparser.NoOptionError):
            raise OptionNotFoundError("Property %r not found in config file %s" % (key, self.source))

    def remove(self, section: str, key: str):
        try:
            with open_locked(self.source, 'r+') as f:
                self.config.read_string(f.read())
                self.config.remove_option(section, key)
                f.seek(0)
                f.truncate()
                self.config.write(f)
        except (configparser.NoSectionError, configparser.NoOptionError):
            raise OptionNotFoundError("Property %r not found in config file %s" % (key, self.source))
