from logging import getLogger
import os
from pathlib import Path

logger = getLogger(__name__)


class MountPoint(object):
    # Path to the source of the bind mount. Relative to service's state directory if not an absolute path.
    source = None  # type: Path
    # Absolute path to the target of the bind mount
    target = None  # type: Path

    owner = "root"
    group = "root"
    mode = None  # type: int
    is_dir = False

    def __init__(self,
                 source: Path = None,
                 target: Path = None,
                 owner: str = None,
                 group: str = None,
                 mode: int = None,
                 is_dir: bool = None):
        if source is not None:
            self.source = source
        if target is not None:
            self.target = target
        if owner is not None:
            self.owner = owner
        if group is not None:
            self.group = group
        if mode is not None:
            self.mode = mode
        if is_dir is not None:
            self.is_dir = is_dir

        self.created_empty_target = False

        if self.mode is None:
            self.mode = 0o750 if self.is_dir else 0o640

    def create(self, uid, gid):
        logger.debug("Creating bind-mount %s", self.source)
        if self.is_dir:
            self.source.mkdir(mode=self.mode)
        else:
            self.source.touch(mode=self.mode)
        os.chown(str(self.source), uid=uid, gid=gid)
