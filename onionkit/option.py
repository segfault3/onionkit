import abc
import os
from logging import getLogger

from onionkit import _
from onionkit.dbus.object import DBusObject
from onionkit.exceptions import OptionNotFoundError
from onionkit.exceptions import ReadOnlyOptionError

# Only required for type hints
from typing import TYPE_CHECKING, Type, Any
if TYPE_CHECKING:
    from onionkit.service import OnionService


PERSISTENT_TORRC = "/usr/share/tor/tor-service-defaults-torrc"
CONFIG_DIR_PREFIX = "config_"

logger = getLogger(__name__)


class OnionServiceOption(DBusObject, metaclass=abc.ABCMeta):
    @property
    def dbus_info(self) -> str:
        return '''
<node>
    <interface name='org.boum.tails.OnionKit.Option'>        
        <method name='Reload'/>        
        <property name="Value" type="v" access="{}">
            <annotation name="org.freedesktop.DBus.Property.EmitsChangedSignal" value="true"/>
        </property>                
        <property name="Name" type="s" access="read"/>
        <property name="NameForDisplay" type="s" access="read"/>        
        <property name="Description" type="s" access="read"/>             
        <property name="Group" type="s" access="read"/>
        <property name="Writable" type="b" access="read"/>
        <property name="Masked" type="b" access="read"/>                
    </interface>
</node>
    '''.format("readwrite" if self.Writable else "read")

    @property
    def dbus_path(self):
        return os.path.join(self.service.dbus_path, self.Name)

    def __init__(self, service: "OnionService"):
        def set_default_value():
            # Set the default value, store, and apply it. We use _value
            # instead of Value here, because we don't want to emit the
            # PropertiesChanged signal during initialization.
            self._value = self.Default
            try:
                self.store()
            except ReadOnlyOptionError:
                pass
            self.apply()

        super().__init__()
        logger.debug("Initializing option '%s.%s", service.Name, self.Name)
        self.service = service

        if self.service.IsInstalled:
            # Service is already installed, so we try to load the stored value
            try:
                self._value = self.load()
            except OptionNotFoundError:
                set_default_value()
        else:
            set_default_value()

        self.register()

    # ----- Exported functions ----- #

    def Reload(self):
        """Reload the option value"""
        logger.debug("Reloading value of option '%s.%s'", self.service.Name, self.Name)
        new_value = self.load()
        if new_value != self._value:
            self._value = new_value
            self.on_value_changed()

    def Reset(self):
        """Reset the option value to the default value"""
        logger.debug("Resetting value of option '%s.%s'", self.service.Name, self.Name)
        self.Value = self.Default

    # ----- Exported properties ----- #

    @property
    def Name(self) -> str:
        """The name of the option"""
        return self.__class__.__name__

    @property
    @abc.abstractmethod
    def NameForDisplay(self) -> str:
        """Localized name of the option for displaying in UIs"""
        pass

    @property
    @abc.abstractmethod
    def Description(self) -> str:
        """A short description of the option that will be displayed in the GUI"""
        pass

    @property
    def Value(self):
        """The option's value. Setting this to a different value will automatically trigger
        on_value_changed, which will store and apply the value"""
        return self._value

    @Value.setter
    def Value(self, value):
        if self.type == bool and type(value) != bool:
            choices = ["true", "false"]
            if value.lower() not in choices:
                raise ValueError("Invalid value %r for option '%s.%s'. Possible values: %r" %
                                 (value, self.service.Name, self.Name, choices))
            value = value.lower() == "true"

        if self._value != value:
            self._value = value
            self.store()
            self.apply()
            self.on_value_changed()

    @property
    @abc.abstractmethod
    def Default(self):
        """The default value of this option"""
        pass

    # whether this option's value can be set via DBus
    Writable = True

    # options with the same group can be grouped together in UIs
    Group = _("Other")

    # masked options should not be displayed as cleartext in UIs and log files
    Masked = False

    # ----- Not exported properties ----- #

    @property
    @abc.abstractmethod
    def type(self) -> Type:
        """The option's type. This is used in load() to convert the value read from file."""
        pass

    # If set to True, the option value will be reloaded after the
    # systemd unit was successfully started. This is useful for
    # options which change when the service starts, for example
    # Mumble's TLS fingerprint.
    reload_after_service_started = False

    # If set to True, the service will not be automatically
    # restarted if this option's value is changed.
    is_applied_without_restarting = False

    # ----- Not exported functions ----- #

    def load(self) -> Any:
        """Load the option's value from the option file. Create the option file if it doesn't exist.
        If the value of this option is stored anywhere else (e.g. in a config file), this function
        should be overwritten and load the value from this other place instead"""
        logger.debug("Loading option '%s.%s'", self.service.Name, self.Name)
        value = self.service.options_file.get(self.Name)
        return self.type(value)

    def store(self):
        """Store the option's value in the option file. If this option has any other stable
        representation (e.g. if it modifies a config file), this function should be overwritten
        and replace this other representation instead."""
        logger.debug("Storing option '%s.%s'", self.service.Name, self.Name)
        self.service.options_file.set(self.Name, self.Value)

    def on_value_changed(self):
        if self.Masked:
            logger.debug("Option '%s.%s' value changed", self.service.Name, self.Name)
        else:
            logger.debug("Option '%s.%s' value changed to %r", self.service.Name, self.Name, self.Value)

        logger.debug("Emitting PropertiesChanged signal for option '%s.%s'", self.service.Name, self.Name)
        self.emit_signal("org.freedesktop.DBus.Properties", "PropertiesChanged", {"Value": self.Value},
                         "a{s" + self.get_variant_type_string() + "}")

        if not self.is_applied_without_restarting and self.service.IsRunning:
            self.service.restart_systemd_service()

    def apply(self):
        """This function should be overwritten if the option will not automatically apply after
        store() is called and the service is restarted (if the option is stored in a config file,
        it will usually be applied automatically by restarting the service).

        By default, this is NOT called during option initialization, because some options should
        not be applied before actually set (e.g. AllowLAN and AllowLocalhost, for which
        applying the default value would result in removing non-existent iptables rules).
        If you want to apply the default value, call self.apply() in __init__()."""
        logger.debug("Applying option '%s.%s'", self.service.Name, self.Name)

    def clean_up(self):
        """This function should be overwritten if something needs to be cleaned up for this option
        when the service is uninstalled."""
        pass

    def get_variant_type_string(self) -> str:
        if isinstance(self.Value, str):
            return "s"
        # It is important that we test bool before int, because isinstance(bool(), int) is True
        if isinstance(self.Value, bool):
            return "b"
        if isinstance(self.Value, int):
            return "i"
        raise TypeError("Option '%s.%s' has unsupported type %r" % (self.service.Name, self.Name, self.type))

    def __str__(self):
        return "%s: %s" % (self.Name, self.Value)
