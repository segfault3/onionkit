onionkit allows setting up and managing onion services via D-Bus. 
It is still under development, in pre-alpha status. **Please don't use it for anything else than testing yet.**


# Install prerequisites

On Debian Stretch, the following packages are required:

    sudo apt install python3-stem python3-yaml python3-sh python3-colorlog python3-cryptography python3-pydbus python3-openssl debootstrap systemd-container

Additionally, we need systemd-container >= 236, which we get from stretch-backports (this also installs systemd from backports):

    sudo apt install -t stretch-backports systemd-container

# Installation

    sudo ./setup.py install

# Usage

    sudo onionkitd

To log debug output to a file:

    sudo onionkitd -l /tmp/onionkit.log

You can control onionkit via [onionservices](https://gitlab.com/segfault3/onionservices) or via the command-line:

```
$ onionkitctl list
- lighttpd
- mumble
- sftp
- gobby
- prosody
$ onionkitctl install mumble
$ onionkitctl list-installed
- mumble
$ onionkitctl start mumble
$ onionkitctl info mumble
description: Voice chat server
installed: true
running: true
published: true
address: 4kalis5mhk3gd77v.onion
options:
  VirtualPort: 64738
  ServerPassword: EfDyH4S8BFux8PHW7r3C
  Persistence: false
  Autostart: false
  AllowLocalhost: true
  AllowLAN: false
  WelcomeMessage: ''
  TLSFingerprint: D2:11:34:43:33:41:CE:10:27:A8:FE:9D:82:7F:C2:40:51:0E:E7:7E
```

