#! /usr/bin/env python3
"""A setuptools based setup module.

Based on:
https://github.com/pypa/sampleproject
"""

# Always prefer setuptools over distutils
try:
    from setuptools import setup
except ImportError:
    from distutils.core import setup

# To use a consistent encoding
from pathlib import Path
from tempfile import TemporaryDirectory
import sys
import glob
import os
import sh

CONTAINER_ADDITIONAL_PACKAGES = ["dbus"]
CONTAINER_DEBIAN_RELEASE = "buster"

here = Path(__file__).parents[0]
data_dir = Path("/usr/share/onionkit")
services_dir = Path(data_dir, "services")
state_dir = Path("/var/lib/onionkit")
config_dir = Path("/etc/onionkit")
base_rootfs = Path(data_dir, "rootfs")

data_dir.mkdir(mode=0o700, parents=True, exist_ok=True)
state_dir.mkdir(mode=0o700, parents=True, exist_ok=True)
config_dir.mkdir(mode=0o755, parents=True, exist_ok=True)

# Get the long description from the README file
long_description = Path(here, 'README.md').read_text(encoding='utf-8')

setup(
    name='onionkit',

    # Versions should comply with PEP440.  For a discussion on single-sourcing
    # the version across setup.py and the project code, see
    # https://packaging.python.org/en/latest/single_source_version.html
    version='0.1',

    description='Allows installing and managing onion services over a D-Bus interface.',
    long_description=long_description,

    # Choose your license
    license='GPLv3',

    # See https://pypi.python.org/pypi?%3Aaction=list_classifiers
    classifiers=[
        # How mature is this project? Common values are
        #   3 - Alpha
        #   4 - Beta
        #   5 - Production/Stable
        'Development Status :: 3 - Alpha',

        # Indicate who your project is intended for
        'Intended Audience :: End Users/Desktop',
        'Topic :: Communications',

        # Pick your license as you wish (should match "license" above)
        'License :: OSI Approved :: GNU General Public License v3 (GPLv3)',

        # Specify the Python versions you support here. In particular, ensure
        # that you indicate whether you support Python 2, Python 3 or both.
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.5',
    ],

    # You can just specify the packages manually here if your project is
    # simple. Or you can use find_packages().
    packages=['onionkit', 'onionkit.options', 'onionkit.data_files', 'onionkit.dbus', 'onionkit.onionkitctl'],

    # If there are data files included in your packages that need to be
    # installed, specify them here.  If using Python 2.6 or less, then these
    # have to be included in MANIFEST.in as well.
    # package_data={}

    # Although 'package_data' is the preferred approach, in some case you may
    # need to place data files outside of your packages. See:
    # http://docs.python.org/3.4/distutils/setupscript.html#installing-additional-files # noqa
    # In this case, 'data_file' will be installed into '<sys.prefix>/my_data'
    data_files=[
        ('/usr/local/sbin', ['scripts/onionkitd']),
        ('/var/lib/polkit-1/localauthority/10-vendor.d', glob.glob('policy/*.pkla')),
        ('/etc/dbus-1/system.d', ['dbus/org.boum.tails.OnionKit.conf']),
        ('/usr/local/lib/systemd/system', glob.glob('systemd/*')),
        (str(services_dir), glob.glob('services/*')),
        (str(config_dir), ['onionkit.conf']),
    ],

    scripts=['scripts/onionkitctl']
)

# Install base container rootfs if we don't run in Tails
if not os.path.exists("/etc/amnesia"):
    if base_rootfs.exists():
        print("WARNING: Not installing base container rootfs: directory '%s' already exists" % base_rootfs)
    else:
        print("Installing base container rootfs")
        with TemporaryDirectory() as tmpdir:
            sh.debootstrap("--include=%s" % ",".join(CONTAINER_ADDITIONAL_PACKAGES), CONTAINER_DEBIAN_RELEASE, tmpdir,
                           _out=sys.stdout.buffer, _err=sys.stderr.buffer)
            base_rootfs.mkdir(mode=0o700, parents=True)
            sh.mv(glob.glob(os.path.join(tmpdir, "*")), base_rootfs)

        # Remove hostname in base container rootfs, which is copied from the host by debootstrap
        hostname = Path(base_rootfs, "etc/hostname")
        if hostname.exists():
            hostname.unlink()
