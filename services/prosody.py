import re
from pathlib import Path
from logging import getLogger

from onionkit import _
from onionkit.service import OnionService
from onionkit.options.virtual_port import VirtualPort
from onionkit.options.persistence import Persistence
from onionkit.options.autostart import Autostart
from onionkit.util import open_locked
from onionkit.option import OnionServiceOption
from onionkit.data_file import DataFile
from onionkit.mountpoint import MountPoint

logger = getLogger(__name__)


class DataDir(MountPoint):
    source = Path("data")
    target = Path("/var/lib/prosody")
    owner = "prosody"
    group = "prosody"
    is_dir = True

data_dir = DataDir()


class ConfigDir(MountPoint):
    source = Path("config")
    target = Path("/etc/prosody")
    owner = "root"
    group = "prosody"
    is_dir = True

config_dir = ConfigDir()


class ProsodyConfigFile(DataFile):
    source = Path(config_dir.source, "prosody.cfg.lua")
    mode = 0o644

    # Users that add additional VirtualHost entries must list them after
    # the default one which will be used for the hidden service.
    default_content = '''
modules_enabled = {
  "roster";   -- Allow users to have a roster.
  "saslauth"; -- Authentication for clients. Required for clients to log in.
  "disco";    -- Service discovery (notifies clients about muc server, etc.).
  "private";  -- Private XML storage (for room bookmarks, etc.).
  "register"; -- Allow users to register on this server using a client and change passwords.
  "posix";    -- POSIX functionality, sends server to background, enables syslog, etc.
};
modules_disabled = {
  --"offline"; -- Store offline messages
  "s2s"; -- Handle server-to-server connections
};
allow_registration = true;
interfaces = { "127.0.0.1" }
daemonize = true;
log = { info = "*syslog"; }
pidfile = "/var/run/prosody/prosody.pid";
-- The following two entries are controlled by onionkit -- do not edit!
Component "conference.localhost" "muc"
VirtualHost "localhost"
'''

    def update(self, *args, count=1):
        logger.debug("Updating prosody config")
        if len(args) == 2 and all(isinstance(x, str) for x in args):
            replacements = [args]
        elif len(args) == 1 and any(isinstance(args[0], t) for t in [tuple, list]):
            replacements = args[0]
        else:
            raise (ValueError('invalid arguments'))

        with open_locked(self.source, 'r+') as f:
            config = f.read()
            for pattern, replacement in replacements:
                config = re.sub(pattern, replacement, config, flags=re.MULTILINE, count=count)
            f.seek(0)
            f.truncate()
            f.write(config)

config_file = ProsodyConfigFile()


class OfflineMessaging(OnionServiceOption):
    NameForDisplay = _("Offline Messaging")
    Description = "When enabled, messages sent to an offline user are delivered the next time they log in"
    Default = True
    type = bool

    def store(self):
        self.service.update_config(
            '^\s*(--)?\s*"offline";',
            '  {}"offline";'.format('--' if self.Value else '')
        )

    def load(self) -> bool:
        return not bool(re.match('^\s*"offline";', config_file.read()))


class Chatroom(OnionServiceOption):
    NameForDisplay = _("Chatroom")
    Description = "Name of the chat room that the clients should join"
    Default = "chat"
    Group = _("Connection")
    type = str

    def on_value_changed(self):
        super().on_value_changed()
        self.service.on_connection_info_changed()


class ProsodyServer(OnionService):
    Name = 'prosody'
    Description = _('Jabber/XMPP server')
    DescriptionLong = _("With Prosody you can write instant messages with others.")
    Documentation = 'file:///usr/share/doc/tails/website/doc/onionkit/prosody.en.html'
    ClientApplication = 'pidgin'
    Icon = 'prosody'

    systemd_service = 'prosody.service'
    packages = ['prosody']
    port = 5222

    options = [
        VirtualPort,
        Persistence,
        Autostart,
        OfflineMessaging,
        Chatroom
    ]

    data_files = [
        config_dir,
        config_file,
        data_dir
    ]

    @property
    def ConnectionInfo(self) -> str:
        if not self.IsInstalled:
            return str()

        chatroom = self.get_option("Chatroom")

        s = self.connection_info_header
        s += _("Application: %s\n") % self.ClientApplicationForDisplay
        s += _("Address: %s\n") % self.Address
        s += _("Port: %s") % self.virtual_port
        if chatroom:
            s += _("\nChatroom: %s") % chatroom
        return s

    def configure_bottom(self):
        self.set_virtual_host(self.Address)

    def update_config(self, *args, count=1):
        config_file.update(*args, count=count)

        if self.IsRunning:
            logger.info("Restarting prosody after config was updated")
            self.Stop()
            self.Start()

    def set_virtual_host(self, address):
        replacements = (
            ('^Component\s.*\s"muc"$',
             'Component "conference.{}" "muc"'.format(address)),
            ('^VirtualHost\s.*$',
             'VirtualHost "{}"'.format(address)),
        )
        self.update_config(replacements)

    def RegenerateAddress(self):
        super().RegenerateAddress()
        self.set_virtual_host(self.Address)


service_class = ProsodyServer
