from pathlib import Path
import random
import string

from onionkit import _
from onionkit import option
from onionkit.options.virtual_port import VirtualPort
from onionkit.options.persistence import Persistence
from onionkit.options.autostart import Autostart
from onionkit.options.allow_localhost import AllowLocalhost
from onionkit.options.allow_lan import AllowLAN
from onionkit.service import OnionService
from onionkit.data_file import IniLikeConfigFile
from onionkit.mountpoint import MountPoint


class DataDir(MountPoint):
    source = Path("data")
    target = Path("/var/lib/infinoted")
    owner = "infinoted"
    group = "infinoted"
    mode = 0o700
    is_dir = True

data_dir = DataDir()
log_file = Path(data_dir.target, "infinoted.log")


class InfinotedConfigFile(IniLikeConfigFile, MountPoint):
    source = Path("infinoted.conf")
    target = Path("/etc/xdg/infinoted.conf")
    owner = "root"
    group = "infinoted"
    default_content = '''# onionkit gobby-service configuration
[infinoted]
root-directory={}
log-file={}
security-policy=no-tls'''.format(data_dir.target, log_file)


# XXX: Remove once infinoted Debian package ships its own systemd unit file (see Debian bug #810865)
class SystemdUnit(IniLikeConfigFile, MountPoint):
    source = Path("infinoted.service")
    target = Path("/etc/systemd/system/infinoted.service")
    mode = 0o644
    default_content = '''[Unit]
Description=collaborative text editor service
Documentation=man:infinoted(1)
After=network.target

[Service]
User=infinoted
EnvironmentFile=-/etc/default/infinoted
ExecStart=/usr/bin/infinoted ${OPTIONS}

[Install]
WantedBy=multi-user.target'''


config_file = InfinotedConfigFile()
systemd_unit_file = SystemdUnit()


class ServerPassword(option.OnionServiceOption):
    DEFAULT_LENGTH = 20
    NameForDisplay = _("Password")
    Description = _("Password required to connect to service")
    Group = _("Connection")
    Masked = True
    type = str

    @property
    def Default(self):
        return ''.join(random.SystemRandom().choice(string.ascii_letters + string.digits) for _ in
                       range(self.DEFAULT_LENGTH))

    def store(self):
        if self.Value:
            config_file.set("infinoted", "password", self.Value)
        else:
            config_file.remove("infinoted", "password")

    def load(self) -> str:
        return config_file.get("infinoted", "password")

    def on_value_changed(self):
        super().on_value_changed()
        self.service.on_connection_info_changed()


class AutosaveInterval(option.OnionServiceOption):
    NameForDisplay = _("Autosave Interval (Sec)")
    Description = _("Interval in seconds to automatically save all open documents")
    Group = _("Advanced")
    Default = 30
    type = int

    def store(self):
        config_file.set("autosave", "interval", str(self.Value))

    def load(self) -> int:
        return int(config_file.get("autosave", "interval"))


class GobbyServer(OnionService):
    Name = "gobby"
    Description = _("Collaborative text editing service")
    DescriptionLong = _("With Gobby you can edit files simultaneously with others.")
    DocumentationURL = "file:///usr/share/doc/tails/website/doc/onionkit/gobby.en.html"
    ClientApplication = "gobby"
    Icon = "gobby-0.5"

    systemd_service = "infinoted.service"
    packages = ["infinoted"]
    port = 6523

    options = [
        VirtualPort,
        ServerPassword,
        Persistence,
        Autostart,
        AllowLocalhost,
        AllowLAN,
        AutosaveInterval,
    ]

    data_files = [
        config_file,
        systemd_unit_file,
        data_dir
    ]

    user = "infinoted"

    def configure_top(self):
        # Create user
        self.container.execute_command("adduser --system --group --home {home} --no-create-home --shell /bin/false "
                                       "--disabled-login {user}".format(home=data_dir.target, user=self.user))

    def configure_bottom(self):
        self.set_option("AllowLocalhost", True)

    @property
    def ConnectionInfo(self) -> str:
        if not self.IsInstalled:
            return str()

        s = self.connection_info_header
        s += _("Application: %s\n") % self.ClientApplicationForDisplay
        s += _("Address: %s:%s\n") % (self.Address, self.virtual_port)
        s += _("Password: %s") % self.options_dict["ServerPassword"].Value
        return s


service_class = GobbyServer
