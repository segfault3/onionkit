from logging import getLogger
from pathlib import Path
import string
import random
import sqlite3
import OpenSSL.crypto
import time

from onionkit import _
from onionkit import option
from onionkit.exceptions import ReadOnlyOptionError, OptionNotFoundError
from onionkit.options.virtual_port import VirtualPort
from onionkit.options.persistence import Persistence
from onionkit.options.autostart import Autostart
from onionkit.options.allow_localhost import AllowLocalhost
from onionkit.options.allow_lan import AllowLAN
from onionkit.service import OnionService
from onionkit.data_file import DataFile
from onionkit.mountpoint import MountPoint

logger = getLogger(__name__)

FINGERPRINT_LOADING_TIMEOUT = 2
DB_NAME = "murmur.sqlite"


class DataDir(MountPoint):
    source = Path("data")
    target = Path("/var/lib/mumble-server")
    owner = "mumble-server"
    group = "mumble-server"
    is_dir = True

data_dir = DataDir()


class MumbleConfigFile(DataFile, MountPoint):
    source = Path("mumble-server.ini")
    target = Path("/etc/mumble-server.ini")
    owner = "root"
    group = "mumble-server"

    @property
    def default_content(self) -> str:
        return self.source.read_text()

config_file = MumbleConfigFile()


class WelcomeMessage(option.OnionServiceOption):
    NameForDisplay = _("Welcome Message")
    Description = _("Welcome message sent to clients when they connect")
    Default = str()
    type = str

    def store(self):
        if self.Value:
            config_file.delete_lines_starting_with("welcometext")
            config_file.insert_line("welcometext=%s\n" % self.Value)
        else:
            config_file.delete_lines_starting_with("welcometext")

    def load(self) -> str:
        line = config_file.get_first_line_starting_with("welcometext")
        if not line:
            raise OptionNotFoundError("Could not find option %r in %r" % (self.Name, config_file.source))
        return line.split('=')[-1].strip()


class ServerPassword(option.OnionServiceOption):
    DEFAULT_LENGTH = 20
    NameForDisplay = _("Password")
    Description = _("Password required to connect to service")
    Group = _("Connection")
    Masked = True
    type = str

    @property
    def Default(self):
        return ''.join(random.SystemRandom().choice(string.ascii_letters + string.digits) for _ in
                       range(self.DEFAULT_LENGTH))

    def store(self):
        logger.debug("Setting '%s.%s'", self.service.Name, self.Name)
        config_file.delete_lines_starting_with("serverpassword")
        config_file.insert_line("serverpassword=%s\n" % self.Value)

    def load(self):
        line = config_file.get_first_line_starting_with("serverpassword")
        if not line:
            raise OptionNotFoundError("Could not find option %r in %r" % (self.Name, config_file.source))
        return line.split('=')[-1].strip()

    def on_value_changed(self):
        super().on_value_changed()
        self.service.on_connection_info_changed()


class TLSFingerprint(option.OnionServiceOption):
    NameForDisplay = _("TLS Fingerprint")
    Description = _("SHA-1 digest of the servers TLS certificate")
    Group = _("Connection")
    Writable = False
    Default = str()
    type = str
    # Mumble automatically generates a certificate when started, so we reload
    # this option after the service started successfully
    reload_after_service_started = True
    is_applied_without_restarting = True

    def store(self):
        raise ReadOnlyOptionError("Option %r can't be modified" % self.Name)

    def load(self) -> str:
        db_file = Path(data_dir.source, DB_NAME)
        if not db_file.exists():
            logger.warning("Could not load TLS certificate of service %r: No such file: %s", self.service.Name, db_file)
            return str()

        cursor = sqlite3.connect(str(db_file)).cursor()
        cert_row = None
        start_time = time.perf_counter()
        while time.perf_counter() - start_time < FINGERPRINT_LOADING_TIMEOUT:
            cursor.execute("SELECT value FROM config WHERE key = 'certificate'")
            cert_row = cursor.fetchone()
            if cert_row:
                break
            time.sleep(0.1)

        if not cert_row:
            logger.warning("Could not load TLS Certificate of service %r: No certificate in database %s (timeout %s)",
                           self.service.Name, db_file, FINGERPRINT_LOADING_TIMEOUT)
            return str()

        cert_string = cert_row[0]
        cert = OpenSSL.crypto.load_certificate(OpenSSL.crypto.FILETYPE_PEM, cert_string)
        return cert.digest("sha1").decode()

    def on_value_changed(self):
        super().on_value_changed()
        self.service.on_connection_info_changed()


class MumbleServer(OnionService):
    Name = "mumble"
    Description = _("Voice chat server")
    DescriptionLong = _("With Mumble you can create VoIP conferences to talk to others.")
    DocumentationURL = "file:///usr/share/doc/tails/website/doc/onionkit/mumble.en.html"
    ClientApplication = "mumble"
    Icon = "mumble"

    systemd_service = "mumble-server.service"
    # On Stretch we have to install libssl1.0-dev to avoid errors, see https://stackoverflow.com/a/42297296
    # XXX:Buster: Remove libssl1.0-dev
    packages = ["mumble-server", "libssl1.0-dev"]
    port = 64738

    options = [
        VirtualPort,
        ServerPassword,
        Persistence,
        Autostart,
        AllowLocalhost,
        AllowLAN,
        WelcomeMessage,
        TLSFingerprint,
    ]

    data_files = [
        config_file,
        data_dir
    ]

    def configure_bottom(self):
        self.set_option("AllowLocalhost", True)

    @property
    def ConnectionInfo(self) -> str:
        if not self.IsInstalled:
            return str()

        s = self.connection_info_header
        s += _("Application: %s\n") % self.ClientApplicationForDisplay
        s += _("Address: %s\n") % self.Address
        s += _("Port: %s\n") % self.virtual_port
        s += _("Password: %s\n") % self.options_dict["ServerPassword"].Value
        s += _("Certificate SHA-1 Fingerprint: %s") % self.options_dict["TLSFingerprint"].Value
        return s


service_class = MumbleServer
