import sh
from pathlib import Path
import random
import string
from logging import getLogger

from onionkit import _
from onionkit.service import OnionService
from onionkit.option import OnionServiceOption
from onionkit.options.virtual_port import VirtualPort
from onionkit.options.persistence import Persistence
from onionkit.options.autostart import Autostart
from onionkit.options.allow_localhost import AllowLocalhost
from onionkit.options.allow_lan import AllowLAN
from onionkit.data_file import DataFile, IniLikeConfigFile
from onionkit.mountpoint import MountPoint

logger = getLogger(__name__)

KEY_NAME = "ssh_host_ed25519_key"


class DataDir(MountPoint):
    source = Path("data")
    target = Path("/var/lib/sftp")
    is_dir = True

data_dir = DataDir()


class SSHDConfigFile(DataFile):
    source = Path(data_dir.source, "sshd_config")

config_file = SSHDConfigFile()


class SystemdUnit(IniLikeConfigFile, MountPoint):
    source = Path("onionkit-sftp.service")
    target = Path("/etc/systemd/system/onionkit-sftp.service")
    mode = 0o644
    default_content = '''[Unit]
Description=OpenBSD Secure Shell server for SFTP in onionkit
After=network.target auditd.service
ConditionPathExists=!/etc/ssh/sshd_not_to_be_run

[Service]
ExecStart=/usr/sbin/sshd -D -f /var/lib/sftp/sshd_config
ExecReload=/bin/kill -HUP $MAINPID
KillMode=process
Restart=on-failure
'''

systemd_unit_file = SystemdUnit()


class ServerPassword(OnionServiceOption):

    # We don't use a super long password here, because the user has to type it. This should still
    # be far beyond crackable via SSH.
    default_length = 10

    NameForDisplay = _("Password")
    Description = _("Password required to connect to service")
    Group = _("Connection")
    Masked = True
    type = str
    is_applied_without_restarting = True

    @property
    def Default(self):
        import logging
        logging.warning("Setting default password")
        return ''.join(random.SystemRandom().choice(string.ascii_letters + string.digits) for _ in
                       range(self.default_length))

    def apply(self):
        super().apply()
        self.service.container.execute_command("echo {}:{} | chpasswd".format(self.service.user, self.Value))

    def on_value_changed(self):
        super().on_value_changed()
        self.service.on_connection_info_changed()


class SFTPServer(OnionService):
    Name = "sftp"
    NameForDisplay = "SFTP"
    Description = _("File sharing service")
    DescriptionLong = _("With SFTP you can share files with others.")
    DocumentationURL = "file:///usr/share/doc/tails/website/doc/onionkit/sftp.en.html"
    ClientApplication = "nautilus"
    ClientApplicationForDisplay = "Files"
    Icon = "network"

    systemd_service = "onionkit-sftp.service"
    packages = ["openssh-server"]
    port = 22000

    options = [
        VirtualPort,
        ServerPassword,
        Persistence,
        Autostart,
        AllowLocalhost,
        AllowLAN,
    ]

    data_files = [
        data_dir,
        config_file,
        systemd_unit_file
    ]

    # SFTP requires that the chroot directory is owned by root and not group writable.
    # In effect, users other than root cannot write in the chroot directory.
    # A workaround is to create a writable subdirectory, in which users then have to
    # change in order to create and edit files. See https://askubuntu.com/a/134442
    # Since we execute SFTP in a container with its own private set of UIDs, it should
    # be safe to just allow users to connect as root, in order to allow writing in the
    # chroot directory.
    # XXX: IMPLEMENT RUNNING CONTAINER WITH PRIVATE UIDs!
    user = "root"

    def __init__(self):
        super().__init__()
        self.chroot_dir = Path(data_dir.target, "chroot")

        config_file.default_content = '''# onionkit sshd for SFTP configuration
        
# Debian standard options
ChallengeResponseAuthentication no
UsePAM yes
X11Forwarding no
PrintMotd no
AcceptEnv LANG LC_*

# onionkit options
AllowUsers {}
HostKey {}
Port {}
AllowTcpForwarding no
AllowStreamLocalForwarding no
PermitRootLogin yes
Subsystem sftp internal-sftp
ForceCommand internal-sftp
ChrootDirectory {}
'''.format(self.user, Path(data_dir.target, KEY_NAME), self.port, self.chroot_dir)

    @property
    def public_key(self) -> str:
        public_key_file = Path(data_dir.source, KEY_NAME).with_suffix(".pub")
        return public_key_file.read_text().strip()

    @property
    def ConnectionInfo(self) -> str:
        if not self.IsInstalled:
            return str()

        s = self.connection_info_header
        s += _("Application: %s\n") % self.ClientApplicationForDisplay
        s += _("Address: sftp://%s:%s\n") % (self.Address, self.virtual_port)
        s += _("Password: %s\n") % self.options_dict["ServerPassword"].Value
        s += _("SSH Public Key: %s") % self.public_key
        return s

    def configure_bottom(self):

        # Create chroot directory
        logger.debug("Creating directory %s", self.chroot_dir)
        self.container.execute_command("install -o root -g {} -m 750 -d {}".format(self.user, self.chroot_dir))

        # Generate new secret key
        secret_key_file = Path(data_dir.source, KEY_NAME)
        logger.debug("Generating new host key %s", secret_key_file)
        if secret_key_file.exists():
            raise FileExistsError("Secret key file %s already exists. Aborting" % secret_key_file)
        sh.ssh_keygen(sh.echo("n"), "-N", "", "-t", "ed25519", "-C", "", "-f", str(secret_key_file))


service_class = SFTPServer
