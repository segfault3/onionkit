from pathlib import Path
from distutils.dir_util import copy_tree

from onionkit import _
from onionkit.service import OnionService
from onionkit.options.virtual_port import VirtualPort
from onionkit.options.persistence import Persistence
from onionkit.options.autostart import Autostart
from onionkit.options.allow_localhost import AllowLocalhost
from onionkit.options.allow_lan import AllowLAN
from onionkit.mountpoint import MountPoint


class ConfigDir(MountPoint):
    source = Path("config")
    target = Path("/etc/lighttpd")
    owner = "root"
    group = "www-data"
    is_dir = True

    def create(self, *args):
        super().create(*args)
        copy_tree(str(self.target), str(self.source))


config_dir = ConfigDir()


class LighttpdServer(OnionService):
    Name = "lighttpd"
    NameForDisplay = "lighttpd"
    Description = _("Lightweight web server")
    DescriptionLong = _("With lighttpd you can host your website.")
    Documentation = "file:///usr/share/doc/tails/website/doc/onionkit/lighttpd.en.html"
    ClientApplication = "tor-browser"
    Icon = "lighttpd"

    systemd_service = "lighttpd.service"
    packages = ["lighttpd"]
    port = 80

    options = [
        VirtualPort,
        Persistence,
        Autostart,
        AllowLocalhost,
        AllowLAN,
    ]

    data_files = [
        config_dir,
        MountPoint(source=Path("html"), target=Path("/var/www/html"), owner="root", group="www-data", is_dir=True)
    ]


service_class = LighttpdServer
